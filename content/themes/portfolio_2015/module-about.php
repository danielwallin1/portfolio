<?php 
	$title = CFS()->get("about-title");
	$subtitle = CFS()->get("about-subtitle");
	$fields = CFS()->get("about-item"); 
?>

<div class="about__wrapper">
	<div class="row">
		<div class="picture">
		  <div class="bg-img small" style="background-image: url('<?php echo get_template_directory_uri(); ?>/assets/img/me.jpg')"></div>
		</div>
		<div class="title">
		  <h2><?php echo $title; ?></h2>
		  <hr>
		  <p class="sub-text"><?php echo withoutlastword($subtitle); ?> 
		  	<span class="emp"><?php echo lastword($subtitle); ?></span>
		  </p>
		</div>
		<?php foreach ($fields as $field) : ?>
			    <div class="large-3 medium-6 columns center">
			          <div class="feature-icon">
			            <i class="fa <?php echo $field["about-itemiconclass"]; ?>"></i> 
			          </div>
			          <h3><?php echo $field["about-itemtitle"]; ?></h3>
			          <p><?php echo $field["about-itemtext"]; ?></p>
			    </div>
		<?php endforeach; ?>
	</div>
</div>	
           
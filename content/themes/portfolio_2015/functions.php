<?php 

	require __DIR__ . '/includes/assets.php';
	require __DIR__ . '/includes/menus.php';
	require __DIR__ . '/includes/images.php';

	add_filter('show_admin_bar', '__return_false');
	add_theme_support( 'post-thumbnails' );

	function create_work_post_type() {
	  register_post_type( 'work',
	    array(
	      'labels' => array(
	        'name' => 'Work',
	        'singular_name' => 'Work',
	        'add_new' => 'Create new',
	        'add_new_item'=> 'Add work',
	        'new_item'=> 'New work',
	        'edit_item'=> 'Edit work',
	        'view_item'=> 'Show work',
	        'all_items'=> 'All work',
	        'search_items'=> 'Search work',
			'not_found'=> 'Found no works',
	        'not_found_in_trash'=> 'Found no work in the thrash',
	      ),
	    'public' => false,
	    'has_archive' => true,
	    'show_ui' => true,
	    'taxonomies' => array('post_tag'), 
	    'supports' => array('title', 'editor', 'thumbnail'),
	    )
	  );
	}
	add_action( 'init', 'create_work_post_type' );


	function withoutlastword ($string) {
		$lastSpacePosition = strrpos($string, ' ');
		$withoutlastword = substr($string, 0, $lastSpacePosition);
		return $withoutlastword; 
	}

	function lastword ($string) {
		$lastword = substr($string, strrpos($string, ' ') + 1);
		return $lastword; 
	}

	
/*	function add_attr($src, $handle){
	    if ($handle != 'requirejs') 
	            return $src;
	    return $src.'" data-main="js/danielwallin.js"';
	}
	add_filter('script_loader_src','add_attr',10,2);*/


	function get_header_menu($echo = true) {
	    $options = array(
	        'theme_location' => 'header-menu',
	        'menu_id'        => 'header-menu',
	        'echo'           =>  true,
	        'depth'          =>  0,
	        'echo'           =>  $echo,
	        'container'      => 'nav'
	    );

	    wp_nav_menu( $options );
	}

	add_action('admin_menu','remove_default_post_type');
	
	function add_menuclass($ulclass) {
   		return preg_replace('/<a /', '<a class="nav-link"', $ulclass);
	}

	add_filter('wp_nav_menu','add_menuclass');

	function remove_default_post_type() {
		remove_menu_page('edit.php');
	}

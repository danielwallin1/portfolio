module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass : {
      dist: {                            
        options: {                       
          style: 'compressed'
        },
        files: {                         
          'style.css': 'assets/scss/style.scss'    
        }
      }

    }, 

    autoprefixer: {

      // prefix the specified file
      single_file: {
        src: 'style.css',
        dest: 'style.css'
      },

    },

    watch: {
      css: {
        files: 'assets/scss/*/*.scss',
        tasks: ['compass', 'autoprefixer'],
        options: {
          livereload: true,
        },
      },
    },


    compass: {
      dist: {
        options: {
          config: 'config.rb', 
          outputStyle: 'compressed'
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');

  // Default task(s).
  grunt.registerTask('default', ['watch']);

};
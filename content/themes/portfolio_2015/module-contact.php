<section class="section__contact" id="contact">
  <?php $form = CFS()->get("form"); ?>
      <div class="row">
        <h2>Contact</h2> 
        <hr>
        <p class="sub-text">Don't hesitate to get in <span class="emp">touch</span></p>
        <div class="large-12 columns">

          <div class="contact-form medium-8 center-block">
            <?php $callout = CFS()->get("callout");  ?>
            <?php if(!empty($callout)) : ?>
              <div class="callout"><?php echo $callout; ?></div> 
            <?php endif; ?>
            <?php echo do_shortcode($form); ?>
          </div>
        </div>
      </div>
</section>
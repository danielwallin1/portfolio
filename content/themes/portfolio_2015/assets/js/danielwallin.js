define(['jqueryeasing', 'fastclick', 'tooltipster', 'Foundation', 'stellar', 'lazyload'], function(easing, fastclick, tooltipster, Foundation, stellar) {


  fastclick.attach(document.body); 
  
  $(".bg-img").lazyload();

  $('.tooltip').tooltipster({
     animation: 'grow',
     delay: 200,
     touchDevices: false,
     trigger: 'hover'
  });  

  var Daniel = Daniel || {};

  Daniel = {

          scroll : function () {

              var navitem = {
                      toggler : $(".menu-item a"), 
                      speed : 1200, 
                      easing : "easeInOutExpo"
              },
              btn = {
                toggler : $(".showcase, .readmore, .project-link"), 
                speed : 1300, 
                easing : "easeInOutCirc"
              }

              function init () {
                  btn.toggler.on("click", function(e){ animate(e, $(this), btn) });
                  navitem.toggler.on("click", function(e){ animate(e, $(this), navitem) });
                  $('html, body').on("scroll mousedown wheel DOMMouseScroll mousewheel keyup touchmove", function () {
                     $(this).stop(); 
                  });       
              }

              function animate (e, $obj, data) {
                    var section = $obj.attr("href"), 
                        el = $(section), 
                        header = $(".site-header");                                  
                    
                    if (el.length == 1) {                      
                      $('html, body').animate(
                        { 'scrollTop': el.offset().top }, {
                            duration: data.speed,
                            easing: data.easing, 
                            complete : function () {     
                                if (header.hasClass("isScrolled"))
                                   header.addClass("isHidden");

                                if (window.matchMedia(Foundation.media_queries.small).matches) 
                                  $("html").removeClass("open");                            
                                   
                            }
                        }); 
                      e.preventDefault();  
                    } else {
                      return; 
                    }
              } 


              return init(); 

          }, 

          canvas : function () {
              var canvas = $("html"), 
                  menubtn = $(".menu-btn");  

              function init () {

                  $(window).on('resize', Foundation.utils.throttle(function(e){
                     if(menubtn.css("display") == 'none')
                       canvas.removeClass("open");
                  }, 300));

                  menubtn.on("click", function (e){
                    console.log("open");
                    toggleNav(); 
                    e.preventDefault(); 
                  }); 
              }

              function toggleNav() {
                  (canvas.hasClass('open')) ? canvas.removeClass('open') : canvas.addClass('open'); 
              }

              return init(); 
          }, 

          sticky : function () {
            var treshold = 64,
                toggleItem = $("header.site-header"),
                toggleItemHeight  = null,
                timer  = null,
                isDisabled  = null;

            function init() {

                var lastScrollTop = 0,
                    self = this,
                    toggleItemHeight = 112;   

                $(window).on("scroll", function (e) {

                    var enabled = isEnabled(); 

                    if (enabled) {
                        var top = $(this).scrollTop();

                        if( top < treshold ) {
                            toggleItem.removeClass('isScrolled isVisible isHidden');
                        } else if (top > lastScrollTop && top > treshold) {
                            toggleItem.addClass('isScrolled isHidden').removeClass('isVisible');
                            timer = clearTimeout(timer);
                            timer = null;
                            timer = setTimeout(function() {toggleItem.removeClass('hide');}, 1000);
                        } else {
                            toggleItem.removeClass('isHidden').addClass('isScrolled isVisible');
                        }

                        lastScrollTop = top;

                    } else {
                        return; 
                    }
                    
                });

            }


            function isEnabled () {
                  if (window.matchMedia(Foundation.media_queries.small).matches) 
                    isDisabled = true;

                  if (window.matchMedia(Foundation.media_queries.large).matches) 
                    isDisabled = false;

                  if( isDisabled ) { return false; } else { return true; }
            }

            return init();
        }, 

        item : function () {

            function init () {
                var item = $(".item"), 
                    container = $(".section__project:first").find(".project__container"); 

                $("body").on("click", ".close", function () {
                   container.addClass("closed").removeClass("active");
                   setTimeout(function () {
                      container.empty(); 
                      container.removeAttr("style");
                   }, 500); 
                });    

                item.on("click", function (e) {
                   e.preventDefault(); 

                   var target = $(this).find(".itemdetails"), 
                       html   = target.html(); 

                   if (container.is(':empty')) {                    
                       container.removeClass("active closed").addClass("hide"); 
                       container.html('<a class="close"><i class="fa fa-times"></i></a>' + html);
                       setTimeout(function() {
                          container.removeClass("hide").addClass("active"); 
                          height = container[0].scrollHeight;
                          container.height(height);
                       }, 500); 

                   } else {
                      container.removeAttr("style");
                      container.addClass("transition");   
                      setTimeout(function () {
                        container.html('<a class="close"><i class="fa fa-times"></i></a>' + html); 
                        container.removeClass("transition");  
                        height = container[0].scrollHeight;
                        container.height(height); 
                      }, 500);                 
                      
                   }    
                   
                });     
            }

            return init(); 

        }, 

        resize : function () {
            
            function init () {
                var header  = $("header"), 
                    content = $("#main"), 
                    footer  = $("#footer");   

                // set padding of content
                content.css("margin-bottom", footer.outerHeight()); 

                // resize stuff
                $(window).on('resize', Foundation.utils.throttle(function(e){
                     content.css("margin-bottom", footer.outerHeight()); 
                }, 300));
   
            }

            return init(); 

        }, 

        refresh : function () {

            function init () {
               var arr = [
                     "a student who graduated with all top scores from Nackademin", 
                     "a indie pop/electro fan",
                     "a golfer",  
                     "one half italian",
                     "quite good at starcraft",
                     "playing centerback in football", 
                     "out of things to say"
                   ], 
                   arrlength = arr.length, 
                   index = 0, 
                   container = $(".refresh:first"), 
                   btn = container.find("a:first"), 
                   text = container.find(".attribute"), 
                   deg = 360,
                   rotate = 360,  
                   clicked = false;    
              
                
               btn.on("click", function (e){ 
                e.preventDefault(); 

                $(this).css({
                  '-webkit-transform':'rotate(' + deg +'deg)',
                  '-moz-transform': 'rotate(' + deg +'deg)',
                  '-o-transform': 'rotate(' + deg +'deg)',
                  '-ms-transform': 'rotate(' + deg +'deg)',
                }); 
                text.removeClass("attribute"); 
                setTimeout(function(){
                  text.addClass("attribute").text(arr[index]); 
                  if (index + 1 >= arrlength)
                    btn.hide();
                  index++;
                }, 300); 

                deg = deg + rotate; 

               }); 

            }

            return init(); 

        }
  }  

  var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
  };

  $(function () {
      $(document).foundation();

      if (!isMobile.any()) {
        $(window).stellar({ 
          horizontalScrolling: false, 
          responsive: false 
        });
      }

      $(window).on('beforeunload', function(){
        $(window).scrollTop(0);
      });

      Daniel.canvas(); 
      Daniel.scroll(); 
      
      Daniel.sticky(); 
      Daniel.item(); 
      Daniel.resize();
      Daniel.refresh();
      
  });


}); 
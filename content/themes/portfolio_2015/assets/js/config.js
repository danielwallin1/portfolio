require.config({

  baseUrl: "/content/themes/portfolio_2015/assets/js/",
  urlArgs: "v=" + (new Date()).getTime(), 
  deps: ["danielwallin"],

  paths: {
    jqueryeasing : "libs/jquery.easing", 
    fastclick    : "libs/fastclick",
    tooltipster  : "libs/jquery.tooltipster",
    lazyload     : "libs/lazyload", 
    Foundation   : "libs/foundation.min", 
    stellar      : "libs/stellar.min"
  },

  shim: {
    Foundation: {
      exports : "Foundation"
    }
  }

});

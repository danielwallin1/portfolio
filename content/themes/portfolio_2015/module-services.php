<?php
	$title = CFS()->get("services-title"); 
	$obj = CFS()->get("services-obj"); 
?>

<div class="service__wrapper">
	<div class="row">
		<h3 class="center columns large-12 secondary-title"><?php //echo withoutlastword($title) . " "; ?>
			<?php echo lastword($title); ?>
		</h3> 
		<dl class="tabs center" data-tab>
		  <?php $index = 1; ?>	
		  <?php $length = count($obj); $length = 12/$length; ?>
		  <?php foreach ($obj as $data) : ?>	
			  <dd class="<?php if($index == 1) echo "active"; ?> large-<?php echo $length; ?> medium-4 small-6">
			  	<a href="#panel<?php echo $index; ?>">
			  		<i class="fa <?php echo $data["service-iconclass"] ?>"></i>
			  	</a>
			  </dd>
		  <?php $index++; ?>	  
		  <?php endforeach; ?>
		</dl> 
		<div class="tabs-content center large-12 columns">
		  <?php $index = 1; ?>	
		  <?php foreach ($obj as $data) : ?>
			  <div class="content <?php if($index == 1) echo "active"; ?>" id="panel<?php echo $index; ?>">
			    <h3><?php echo $data["service"]; ?></h3>
			    <p><?php echo $data["service-text"]; ?></p>
			  </div>
		  <?php $index++; ?>		  
		  <?php endforeach; ?>
		</div> 
	</div>
</div>
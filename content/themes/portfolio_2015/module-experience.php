<?php 
	$title = CFS()->get("exp-title");
	$type = CFS()->get("exp-type");
	$expobj = CFS()->get("exp-typeobj");
	$skilltitle = CFS()->get("skill-title");
	$skillobj = CFS()->get("skill-obj");
	$clienttitle = CFS()->get("clients-title");
	$clientobj = CFS()->get("clients-obj");
	$educationtitle = CFS()->get("edu-title");
	$eduobj = CFS()->get("edu-obj");
	$edudesc = CFS()->get("education-text");
	$workdesc = CFS()->get("work-text");
?>

<div class="experience__wrapper">
	
	<div class="row">
		
		<h3 class="large-12 columns secondary-title center">Experience</h3>

		<div class="large-12 medium-12 experiences center-mob">

		        <div class="large-3 medium-3 columns hide-for-small-only">
		          <h4><?php echo $type; ?></h4>
		        </div>

		        <div class="large-5 medium-5 columns">
		          <h4 class="hide-for-medium-up"><?php echo $type; ?></h4>	
		          <ul>
		              <?php foreach ($expobj as $data) : ?>
		              	<li>
		              		<strong><?php echo $data["exp-work"]; ?></strong> 
		              		<em><?php echo $data["exp-date"]; ?></em> 
		              		<span><?php echo $data["exp-role"]; ?></span> 
		              	</li>
		          	  <?php endforeach; ?>	
		          </ul> 
		        </div>		       	

		        <div class="large-4 medium-4 columns">
		        	<h4><?php echo $skilltitle; ?></h4>
		            <ul>
		                <?php foreach ($skillobj as $data) : ?>
		                	<li><i class="fa fa-check"></i> <?php echo $data["skill"]; ?></li>
		               	<?php endforeach; ?>
		            </ul> 
        		</div>

		        
		</div>

		<div class="large-12 medium-12 experiences center-mob">

			<div class="large-3 medium-3 columns hide-for-small-only">
		        <h4><?php echo $educationtitle; ?></h4>         
		    </div>

		    <div class="large-5 medium-5 columns">
		    	  <h4 class="hide-for-medium-up"><?php echo $educationtitle; ?></h4>	
		          <ul>
		              <?php foreach ($eduobj as $data) : ?>
		              	<li>
		              		<strong><?php echo $data["edu-school"]; ?></strong> 
		              		<em><?php echo $data["edu-date"]; ?></em> 
		              		<span><?php echo $data["edu-program"]; ?></span> 
		              	</li>
		          	  <?php endforeach; ?>	
		          </ul> 
		    </div>
			
        	<div class="large-4 medium-4 columns clients">
        		<h4><?php echo $clienttitle; ?></h4>
	            <ul>	               
	                <?php foreach ($clientobj as $data) : ?>
	                	<li><?php echo $data["client"]; ?></li>
	               	<?php endforeach; ?>
	            </ul> 
	        </div>    
      </div>

     <div class="large-12 columns"> 
	     <div class="large-6 medium-12 columns center">
		      <h3 class="center">Work & internships</h3>
		      <p><?php echo $workdesc; ?></p>
	     </div>  
	     <div class="large-6 medium-12 columns center">
		      <h3 class="center">Education</h3>
		      <p><?php echo $edudesc; ?></p>
	    </div>
    </div> 
	</div>

</div>

<?php get_header(); ?>

<section class="fourofour large-12 columns">
  <div class="row">
	<h1 class="center">404</h1>
	<h2>We couldn't find the page you were looking for</h2>
	<p class="center full"><a href="<?php echo get_home_url(); ?>" class="btn btn-custom">Home page</a></p>
  </div>  
</section>

<?php get_footer(); ?>
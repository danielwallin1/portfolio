<?php // Register Navigation Menus
function dw_navigation_menus() {
    $locations = array(
        'header-menu' => 'Header menu',
        'footer-menu' => 'Footer menu',         
    );

    register_nav_menus( $locations );
}

// Hook into the 'init' action
add_action( 'init', 'dw_navigation_menus' );

?>
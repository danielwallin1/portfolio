<?php
	
	function dw_admin_stylesheet() {
	    wp_enqueue_style( 'custom-mce-style', get_template_directory_uri() . '/admin.css' );
	}

	add_action( 'admin_enqueue_scripts', 'dw_admin_stylesheet' );
	
	
	function dw_register_styles() {
		wp_enqueue_style( 'main-stylesheet', get_stylesheet_uri() );
		wp_enqueue_style( 'fontawesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );
		wp_enqueue_style( 'lobster', 'http://fonts.googleapis.com/css?family=Lobster' ); 
		wp_enqueue_style( 'open-sans-condensed', 'http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700' );
		wp_enqueue_style( 'open-sans', 'http://fonts.googleapis.com/css?family=Open+Sans' );
		wp_enqueue_style( 'tooltipster', get_template_directory_uri() . "/includes/stylesheets/tooltipster.css" );
	}

	add_action( 'wp_enqueue_scripts', 'dw_register_styles' );

	function dw_register_scripts() {
		wp_enqueue_script( 'jQuery', "http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" , '1.0.0', true );
	}

	add_action( 'wp_enqueue_scripts', 'dw_register_scripts' );
?>
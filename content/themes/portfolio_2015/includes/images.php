<?php

if ( function_exists( 'add_image_size' ) ) {
	// 16:9 format for content
	add_image_size( 'medium_thumb', 640, 480 );

}
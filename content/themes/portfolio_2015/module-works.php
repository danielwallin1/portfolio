<?php 
    $args = array(
      'post_type' => 'work', 
      'post_status' => 'publish', 
      'orderby' => 'menu_order', 
      'posts_per_page' => -1
    ); 

    $data = get_posts($args);
?>  

<section class="section__grid" id="work">
          <h2>The lab</h2>
          <hr>
            <p class="sub-text">I love creating <span class="emp">things</span></p>
            <section class="section__project" id="project">
                <div class="row">
                    <div class="project__container"></div>
                </div>
            </section>
            <div class="grid__content large-12 columns">

              <?php foreach ($data as $key) : ?>
                <?php $detailimg = CFS()->get("work-img", $key->ID); 
                      $detailimg = wp_get_attachment_image_src($detailimg, "large");  
                      $imgurl = wp_get_attachment_image_src( get_post_thumbnail_id($key->ID), "medium"); 
                      $img = (!empty($detailimg[0])) ? $detailimg[0] : $imgurl[0];
                      $imgsize = CFS()->get("work-imgwidth", $key->ID); 
                      $class = (!empty($imgsize)) ? array_values($imgsize) : "4";  
                 ?> 
                <div class="item large-<?php echo $class[0]; ?> medium-6 columns">
                   <a href="#project" class="project-link">
                      <div class="bg-img" data-original="<?php echo $imgurl[0]; ?>" style="background-image: url('<?php echo $imgurl[0]; ?>')"></div>
                      <div class="overlay">
                        <div>
                          <div class="overlay-content">
                            <h4 class="center"><?php echo get_the_title( $key->ID ); ?></h4>
                            <i class="fa fa-link feature-link"></i>
                          </div>
                        </div>
                      </div>
                   </a>   
                    <div class="itemdetails hide">
                      <div class="row">
                        <div class="large-12 columns">
                          <div class="large-8 columns">

                           <div class="bg-img" style="background-image: url('<?php echo $img; ?>');"></div>
                          </div>
                          <div class="large-4 columns">
                            <h3><?php echo $key->post_title; ?></h3>
                            <p><?php echo $key->post_content; ?></p>
                            <h3>Project details</h3>
                            <p class="proj-detail">
                              <span class="bold condensed uppercase">Client:</span> 
                              <?php echo CFS()->get("work-client", $key->ID); ?>
                            </p>
                            <p class="proj-detail">
                              <span class="bold condensed uppercase">Date:</span> 
                              <?php echo CFS()->get("work-date", $key->ID); ?>
                            </p>
                            <p class="proj-detail">
                              <?php $tags = get_the_tags($key->ID); ?>
                              <span class="bold condensed uppercase">Tags:</span> 
                              <?php if (!empty($tags))
                                foreach ($tags as $tag) {
                                    echo $tag->name . " ";
                                }
                              ?>
                            </p>
                            <?php $link = CFS()->get("work-link", $key->ID); ?>
                            <?php if (!empty($link)) : ?>
                              <a href="<?php echo $link; ?>" class="btn btn-custom" target="_blank">Visit the site <i class="fa fa-external-link icon"></i></a>
                            <?php endif; ?>
                          </div>
                        </div>

                      </div>
                    </div> 
                </div>       

              <?php endforeach; ?>   

           </div>    
</section>

<header class="site-header">
  <div class="row">
    <?php get_header_menu(); ?>
  </div>
</header>  

<!-- start main content -->
<div class="hide-for-large-up menu-btn btn btn-empty">
    <div class="_layer -top"></div>
    <div class="_layer -mid"></div>
    <div class="_layer -bottom"></div>
</div>
<div class="content" id="main">
        <section class="section__parallax" data-stellar-background-ratio="0.5">
            <div class="row section__header">
              
              <div class="large-12 columns section__headerinner">                  
                  <div class="heading__details">                      
                    <h1>Hi, Im Daniel Wallin</h1>
                    <h2 class="refresh">Im <span class="attribute">a developer and designer!</span><a href="#" class="refreshbtn"><i class="fa fa-refresh"></i></a></h2>
                    <div class="btn-holder">
                      <?php if (is_front_page()) : ?>
<!--                         <a href="#about" class="btn btn-custom readmore">Read more</a>   -->
                        <a href="#work" class="btn btn-empty showcase">View my work</a> 
                      <?php endif; ?>  
                    </div>  
                    
                  </div>
              </div>
            </div>
            <div class="bgoverlay"></div>
        </section>
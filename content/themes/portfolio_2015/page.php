<?php get_header(); ?>

<section class="section__about" id="about">
	<?php get_template_part("module", "about"); ?>
	<?php get_template_part("module", "experience"); ?>
	<?php get_template_part("module", "services"); ?>
</section>	

<?php get_template_part("module", "works"); ?>

<?php get_template_part("module", "contact"); ?>

<?php get_footer(); ?>


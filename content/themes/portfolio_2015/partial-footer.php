<footer id="footer">

    <div class="row">

        <div class="large-4 medium-4 small-12 columns">
            <p class="call">Call me :  +46 739 63 9595</p> 
        </div>  

        <div class="large-4 medium-4 small-12 columns center">
          <a href="" class="name">Daniel Wallin</a>
          <ul class="social">
                <li><a href="http://se.linkedin.com/in/danielwallin89" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="https://www.facebook.com/danielwallin89" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://bitbucket.org/danielwallin1/portfolio/commits/all" target="_blank" class="radius tooltip" title="This project is available on Bitbucket!"><i class="fa fa-bitbucket"></i></a> </li>
              </ul>  
        </div>

        <div class="large-4 medium-4 small-12 columns">
             <p class="adress">Stockholm, Sweden</p>
        </div> 

    </div>

    <div class="footer-down">
        <div class="row">
          <p class="copy">© <?php echo date('Y'); ?> Daniel Wallin. ALL RIGHTS RESERVED.</p>
        </div>
    </div>

</footer> 